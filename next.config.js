/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images:{
    domains: ['zhamagat.kz'],
  }
}

module.exports = nextConfig
