import React, {useEffect, useState} from 'react';
import styles from './mosque.module.css'
import Link from "next/link";
import Image, {StaticImageData} from "next/image";
import arrRight from "@/assets/arrRight.svg"
import {ILesson} from "@/components/filter/filter";

type MosqueProps ={
    id: number;
    name: string;
    image: string;
    lessons: ILesson[];
};

const Mosque = ({mosqueInput}: {mosqueInput: MosqueProps }) => {
    const [gender, setGender] = useState('')
    useEffect(()=>{
        if (typeof window !== 'undefined') {
            let item = localStorage.getItem('gender');
            setGender(item+'')
        }
    },[])
    return (
        <div className={styles.mosque}>
            <div className={styles.mosque__img}>
                <img
                    src={mosqueInput.image}
                    alt={`${mosqueInput.name} Image`}
                />
            </div>
            <div className={styles.mosque__info}>
                <div className={styles.mosque__title}>
                    <h3>
                        {mosqueInput.name}
                    </h3>
                    <Link href={`/${mosqueInput.name}/??gender=${gender}&?mosque=${mosqueInput.id}`}>
                        <div className={styles.mosque__link}>
                            <p>толық тізім</p>
                            <Image src={arrRight} alt="arrow right"/>
                        </div>
                    </Link>
                </div>
                <div className={styles.mosque__schedule}>
                    {mosqueInput.lessons.map((item,index)=>index<2&&(
                        <div key={`home schedule ${index}`}>
                            <span>{item.date} </span>
                            <span>{item.start_time.slice(0,5)}-{item.end_time.slice(0,5)}</span>
                        </div>
                    ))}
                </div>
            </div>

        </div>
    );
};

export default Mosque;
