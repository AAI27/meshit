import React, { useState } from 'react';
import arrLeft from "@/assets/arrLeft.svg"
import arrRight from "@/assets/arrRight.svg"
import Image from "next/image";
import styles from "./calendar.module.css";
import {Button} from "antd";

type CalendarProps = {
    close: (arg: string) => void;
};
const Calendar = ({ close }: CalendarProps ) => {
    const [currentDate, setCurrentDate] = useState(new Date());
    const [selectedDay, setSelectedDay] = useState(0)
    const [selectedDate, setSelectedDate] = useState('')

    const month = [
        "Қаңтар",
        "Ақпан",
        "Наурыз",
        "Сәуір",
        "Мамыр",
        "Маусым",
        "Шілде",
        "Тамыз",
        "Қыркүйек",
        "Қазан",
        "Қараша",
        "Желтоқсан"
    ];

    const getMonthCalendar = () => {
        const year = currentDate.getFullYear();
        const month = currentDate.getMonth();
        const firstDay = new Date(year, month, 1).getDay() - 1;
        const daysInMonth = new Date(year, month + 1, 0).getDate();

        const calendar = [];
        let dayCounter = 1;

        // Добавление пустых ячеек до первого дня месяца
        for (let i = 0; i < firstDay; i++) {
            const prevMonth = month === 0 ? 11 : month - 1;
            const prevMonthYear = month === 0 ? year - 1 : year;
            const prevMonthDays = new Date(prevMonthYear, prevMonth + 1, 0).getDate();
            const day = prevMonthDays - firstDay + i + 1;

            calendar.push(
                <div
                    key={`empty-${i}`}
                    className={`${styles.calendar_day} ${styles.empty}`}
                    onClick={() => handleDayClick(new Date(prevMonthYear, prevMonth, day))}
                >
                    {day}
                </div>
            );
        }

        while (dayCounter <= daysInMonth) {
            const date = new Date(year, month, dayCounter);
            const isWeekend = date.getDay() === 0 || date.getDay() === 6;

            calendar.push(
                <div
                    key={`day-${dayCounter}`}
                    className={`${styles.calendar_day} ${selectedDay === date.getDate() ? styles.current_day : ''} ${isWeekend ? styles.weekend : ''}`}
                    onClick={() => handleDayClick(date)}
                >
                    {dayCounter}
                </div>
            );

            dayCounter++;
        }

        // Добавление пустых ячеек после последнего дня месяца
        const remainingEmptyDays = (7 - (calendar.length % 7)) % 7;
        for (let i = 0; i < remainingEmptyDays; i++) {
            const nextMonth = month === 11 ? 0 : month + 1;
            const nextMonthYear = month === 11 ? year + 1 : year;
            const day = i + 1;

            calendar.push(
                <div
                    key={`empty-${dayCounter}`}
                    className={`${styles.calendar_day} ${styles.empty}`}
                    onClick={() => handleDayClick(new Date(nextMonthYear, nextMonth, day))}
                >
                    {day}
                </div>
            );

            dayCounter++;
        }

        return calendar;
    };

    const handleDayClick = (date:Date) => {
        const year = date.getFullYear();
        const month = (date.getMonth() + 1).toString().padStart(2, '0');
        const day = date.getDate().toString().padStart(2, '0');
        const formattedDate = `${year}-${month}-${day}`;
        setSelectedDay(date.getDate())
        setSelectedDate(formattedDate)

    };

    const prevMonth = () => {
        setCurrentDate((prevDate) => {
            return new Date(prevDate.getFullYear(), prevDate.getMonth() - 1, 1);
        });
    };

    const nextMonth = () => {
        setCurrentDate((prevDate) => {
            return new Date(prevDate.getFullYear(), prevDate.getMonth() + 1, 1);
        });
    };

    return (
        <div className={styles.calendar}>
            <div className={styles.calendar_header}>
                <h3>{month[currentDate.getMonth()]} {currentDate.getFullYear()}</h3>
                <div className={styles.calendar_img}>
                    <Image src={arrLeft} alt="arrLeft icon" onClick={prevMonth} />
                    <Image src={arrRight} alt="arrRight icon" onClick={nextMonth} />
                </div>
            </div>
            <div className={styles.calendar_grid}>
                <div className={styles.calendar_day}>Пн</div>
                <div className={styles.calendar_day}>Вт</div>
                <div className={styles.calendar_day}>Ср</div>
                <div className={styles.calendar_day}>Чт</div>
                <div className={styles.calendar_day}>Пт</div>
                <div className={`${styles.calendar_day} ${styles.weekend}`}>Сб</div>
                <div className={`${styles.calendar_day} ${styles.weekend}`}>Вс</div>
                {getMonthCalendar()}
            </div>
            <Button onClick={()=>{close(selectedDate)}} type="primary" size={"large"} style={{marginTop:20,backgroundColor:"#4CAF50"}} block>
                Көрсету
            </Button>
        </div>
    );
};

export default Calendar;
