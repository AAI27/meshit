import React, { useState } from 'react';
import style from './gender.module.css';
import FemaleImg from '@/assets/OBJECTS.jpg';
import MaleImg from '@/assets/OBJECTS (1).jpg';
import Image from 'next/image';
import styles from "@/components/filter/filter.module.css";

const Gender = ({close}:{close:(bool:boolean)=>void}) => {
    const [gender, setGender] = useState<string>('MALE');


    const clickHandle = (value: string) => {
        setGender(value);
    };
    const onClose = () =>{
        close(false)
        if (typeof window !== 'undefined') {
            localStorage.setItem('gender', gender);
        }
    }

    return (
        <div className={style.gender}>
            <div className={style.gender__container}>
                <label className={style.genderLabel}>
                    <input
                        type="checkbox"
                        value="man"
                        name="gender"
                        onChange={() => {
                            clickHandle('MALE');
                        }}
                        checked={gender === 'MALE'}
                    />
                    <div className={style.gender__input}>
                        <Image src={MaleImg} alt="Male" />
                        <p>Er</p>
                    </div>
                </label>
                <label className={style.genderLabel}>
                    <input
                        type="checkbox"
                        name="gender"
                        value="Woman"
                        onChange={() => {
                            clickHandle('FEMALE');
                        }}
                        checked={gender === 'FEMALE'}
                    />
                    <div className={style.gender__input}>
                        <Image src={FemaleImg} alt="Female" />
                        <p>Ayel</p>
                    </div>
                </label>
            </div>
            <button onClick={onClose}  className={styles.filter__btn}>
                Таңдау
            </button>
        </div>
    );
};

export default Gender;
