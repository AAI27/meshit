import React, {Dispatch, SetStateAction, useEffect, useState} from 'react';
import styles from './filter.module.css';
import Image from 'next/image';
import ArrowIcon from '../../assets/Arrow - Left.svg';
import ArrRight from '../../assets/arrRight.svg';
import { useRouter } from 'next/router';

export type ITypes = {
    id:number,
    name:string
}
export type ILesson = {
    id:number,
    name:string,
    type: {
        id: number,
        name: string
    },
    teacher: {
        id: number,
        name: string,
    },
    start_time: string,
    end_time: string,
    date: string
}

type FilterProps = {
    isOpen: boolean,
    setIsOpen: Dispatch<SetStateAction<boolean>>,
    cityGiven: {id:number, name:string}[],
    city:number,
    setCity: Dispatch<SetStateAction<number>>,
    mosquesGiven: ILesson[],
};

const Filter = ({ isOpen, setIsOpen, cityGiven , mosquesGiven, city, setCity }:FilterProps) => {
    const router = useRouter();

    const [isMan, setIsMan] = useState('MALE');
    const [mosque, setMosque] = useState('');
    const [selectedActivity, setSelectedActivity] = useState('');

    const [activityTypeGiven, setActivityTypeGiven] = useState<ITypes[]>([]);


    const closeFilter = () => {
        setIsOpen(false);
    };
    const handleSubmit = () => {
        let str=''
        str+=`gender=${isMan}&`
        str+=`city=${city}&`
        str+=!!mosque?`mosque=${mosque}&`:''
        str+=!!selectedActivity?`type=${selectedActivity}`:''

        if (typeof window !== 'undefined') {
            localStorage.setItem('gender', isMan);
        }
        router.push(`/Filter/?${str}`)
    };
    const handleClear = () =>{
    }

    useEffect(() => {
        const fetchTypes = async () => {
            try {
                const response = await fetch(`https://zhamagat.kz/api/types`);
                const data = await response.json();

                setActivityTypeGiven(data);
            } catch (error) {
                console.error("Error fetching mosques:", error);
            }
        };
        fetchTypes();
    }, []);

    return (
        <div className={`${styles.filter} ${isOpen ? styles.active : ''}`}>
            <div className={styles.filter__header}>
                <Image onClick={closeFilter} src={ArrowIcon} alt="arrow-icon" />
                <div>
                    <h2>Фильтры</h2>
                </div>
                <div>
                    <button className={styles.clear__btn} onClick={handleClear}>Тазалау</button>
                </div>
            </div>
            <hr />
            <div className={styles.filter__form}>
                <div className={styles.filter__type}>
                    <div className={styles.filter__name}>
                        <h3>Жынысы</h3>
                        <Image src={ArrRight} alt="arrow icon right" />
                    </div>
                    <div className={`${styles.filter__section} ${styles.filter__gender}`}>
                        <label className={styles.checkboxLabel}>
                            <input
                                type="checkbox"
                                value="MALE"
                                name="gender"
                                onChange={() => {
                                    setIsMan('MALE');
                                }}
                                className={styles.filter__checkbox}
                                checked={isMan==='MALE'}
                            />
                            <span>Ер</span>
                        </label>
                        <label className={styles.checkboxLabel}>
                            <input
                                type="checkbox"
                                name="gender"
                                value="FEMALE"
                                onChange={() => {
                                    setIsMan('FEMALE');
                                }}
                                className={styles.filter__checkbox}
                                checked={isMan==='FEMALE'}
                            />
                            <span>Әйел</span>
                        </label>
                    </div>
                    <hr />
                </div>
                <div className={styles.filter__type}>
                    <div className={styles.filter__name}>
                        <h3>Қала</h3>
                        <Image src={ArrRight} alt="arrow icon right" />
                    </div>
                    <div className={styles.filter__scroll}>
                        <div className={styles.filter__section}>
                            {cityGiven.map((item, index) => (
                                <label
                                    key={`key${item}${index}`}
                                    className={styles.checkboxLabel}
                                >
                                    <input
                                        type="checkbox"
                                        value={item.name}
                                        onChange={(event) => {
                                            setCity( item.id);
                                            setMosque('')
                                        }}
                                        className={styles.filter__checkbox}
                                        checked={city===item.id}
                                    />
                                    <span>{item.name}</span>
                                </label>
                            ))}
                        </div>
                    </div>
                    <hr />
                </div>
                <div className={styles.filter__type}>
                    <div className={styles.filter__name}>
                        <h3>Мешіттер</h3>
                        <Image src={ArrRight} alt="arrow icon right" />
                    </div>
                    <div className={styles.filter__scroll}>
                        <div className={styles.filter__section}>
                            {mosquesGiven.map((item, index) => (
                                <label
                                    key={`key${item}${index}`}
                                    className={styles.checkboxLabel}
                                >
                                    <input
                                        type="checkbox"
                                        value={item.id}
                                        onChange={(event) => {
                                            setMosque(item.id+'')
                                        }}
                                        className={styles.filter__checkbox}
                                        checked={mosque===item.id+''}
                                    />
                                    <span>{item.name}</span>
                                </label>
                            ))}
                        </div>
                    </div>
                    <hr />
                </div>
                <div className={styles.filter__type}>
                    <div className={styles.filter__name}>
                        <h3>Іс шара түрлері</h3>
                        <Image src={ArrRight} alt="arrow icon right" />
                    </div>
                    <div className={styles.filter__scroll}>
                        <div className={styles.filter__section}>
                            {activityTypeGiven.map((item, index) => (
                                <label
                                    key={`key${item.id}${index}`}
                                    className={styles.checkboxLabel}
                                >
                                    <input
                                        type="checkbox"
                                        value={item.id}
                                        onChange={() => {
                                            setSelectedActivity(item.id+'')
                                        }}
                                        className={styles.filter__checkbox}
                                        checked={selectedActivity===item.id+''}
                                    />
                                    <span>{item.name}</span>
                                </label>
                            ))}
                        </div>
                    </div>
                    <hr />
                </div>
                {/*<div className={styles.filter__type}>*/}
                {/*    <div className={styles.filter__name}>*/}
                {/*        <h3>Іс шара уақыттары</h3>*/}
                {/*        <Image src={ArrRight} alt="arrow icon right" />*/}
                {/*    </div>*/}
                {/*    <div className={styles.filter__scroll}>*/}
                {/*        <div className={styles.filter__section}>*/}
                {/*            {activityTimeGiven.map((item, index) => (*/}
                {/*                <label*/}
                {/*                    key={`key${item}${index}`}*/}
                {/*                    className={styles.checkboxLabel}*/}
                {/*                >*/}
                {/*                    <input*/}
                {/*                        type="checkbox"*/}
                {/*                        value={item}*/}
                {/*                        onChange={(event) => {*/}
                {/*                            addToForm('activityTime', item);*/}
                {/*                        }}*/}
                {/*                        className={styles.filter__checkbox}*/}
                {/*                        checked={filterForm.activityTime.includes(item)}*/}
                {/*                    />*/}
                {/*                    <span>{item}</span>*/}
                {/*                </label>*/}
                {/*            ))}*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*    <hr />*/}
                {/*</div>*/}
            </div>
            <button onClick={handleSubmit} className={styles.filter__btn}>
                Көрсету
            </button>
        </div>
    );
};

export default Filter;
