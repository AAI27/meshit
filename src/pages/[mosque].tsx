import React, { useEffect, useState } from 'react';
import {Divider, Layout, List, Modal} from 'antd';
import Link from "next/link";
import Image from "next/image";
import ArrowIcon from "@/assets/Arrow - Left.svg";
import CalendarIcon from "@/assets/Calendar.svg";
import {useRouter} from "next/router";
import Calendar from "@/components/calendar/calendar";
import {ILesson} from "@/components/filter/filter";


const Mosque: React.FC = () => {
    const router = useRouter();
    const mosque = router.query
    const name = mosque.mosque
    const joinedString = Object.entries(mosque)
        .map(([key, value],index) => key!=='mosque'?`${key}=${value}`:'')
        .join('&');

    function showToday() {
        const today = new Date();
        const year = today.getFullYear();
        const month = String(today.getMonth() + 1).padStart(2, '0');
        const day = String(today.getDate()).padStart(2, '0');

        return `${year}-${month}-${day}`;
    }
    const [date,setDate] = useState(showToday)
    const [lessons, setLessons] =useState<ILesson[]>([])
    const { Header, Content } = Layout;
    const [calendarShow, setCalendarShow] = useState(false)
    const [selectedDate, setSelectedDate] = useState(null)
    const [loading, setLoading] = useState(false);


    const showModal = () => {
        setCalendarShow(true);
    };
    const hideModal = (date:string) => {
        if(date){
            setDate(date)
        }
        setCalendarShow(false);
    };

    const loadMoreData = () => {
        if (loading) {
            return;
        }
    };
    useEffect(() => {
        const fetchMosquesById = async () => {
            try {
                const response = await fetch(`https://zhamagat.kz/api/lessons/?date=${date}&${joinedString}`);
                const data = await response.json();
                setLessons(data);
            } catch (error) {
                console.error("Error fetching mosques:", error);
            }
        };
        fetchMosquesById();
    }, [date,joinedString]);

    return (
        <Layout className="container">
            <Header style={{backgroundColor:"white",height: 50, alignItems:"center",padding:20}}>
                <Link href={'/'} style={{display:"flex",alignItems:"center"}}>
                    <Image
                        src={ArrowIcon}
                        alt="Back Icon"
                    />
                </Link>
                <div>
                    {/*style={}
                                    key={item.date}*/}
                    <h2>{name}</h2>
                </div>
                <Image
                    onClick={showModal}
                    src={CalendarIcon}
                    alt="Calendar Icon"
                    width={30}
                    height={30}
                />
            </Header>
            <Content>
                <Divider style={{color:"gray"}}>{date}</Divider>


                        <List
                            dataSource={lessons}
                            style={{border:"none",flexDirection:"column",alignItems:"start",paddingLeft:"15px"}}
                            renderItem={(item) => (
                                <p style={{fontSize:15}}>
                                    {item.start_time.slice(0,5)}-{item.end_time.slice(0,5)} - <strong>{item.type.name}</strong>
                                </p>
                            )}
                        />
                <Modal
                    title=""
                    open={calendarShow}
                    footer={null}
                    width={350}
                    closable={false}
                >
                    <Calendar close={hideModal}/>
                </Modal>
            </Content>
        </Layout>
    );
};

export default Mosque;
