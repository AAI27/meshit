import Image from "next/image";
import FilterIcon from "../assets/filterIcon.svg"
import Mosque from "../components/mosque/mosque";
import React, {useEffect, useState,useRef} from "react";
import Filter from "../components/filter/filter";
import {Select, Input, Carousel, Layout, Modal, Skeleton} from "antd";
import Gender from "@/components/gender/gender";
import {BaseSelectRef} from "rc-select";


export type IBanners = {
  id: number,
  image: string,
  lesson: number
}
export type IMosque = {
  "id": 4,
  "name": "Нұр-Мубарак мешіті",
  "image": "https://zhamagat.kz/media/mosques/25317552.jpg",
  "lessons": []
}
export type ICities = {
  id:number,
  name:string
}


export default function Home()  {
  const [search,setSearch]=useState(false);
  const [cities,setCities]=useState<ICities[]>([])
  const [selectedCity,setSelectedCity]=useState(1)
  const [filterShow,setFilterShow]=useState(false);

  const [mosques, setMosques] = useState([]);
  const [images, setImages] = useState<IBanners[]>([]);
  const [genderModal, setGenderModal]= useState(false)

  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingBanner, setIsLoadingBanner] = useState(true);


  const selectRef = useRef<BaseSelectRef | null>(null);


  const { Header, Content } = Layout;


  let isSelected;


  const handleChange = (option:number) => {
    setSelectedCity(option)
    setIsLoading(true)
  };

  const onSearch = (event: React.KeyboardEvent<HTMLInputElement>) => {
  };
  useEffect(() => {
    const fetchBanners = async () => {
      try {
        const response = await fetch("https://zhamagat.kz/api/banners/");
        const data = await response.json();
        setImages(data);
        setIsLoadingBanner(false)
      } catch (error) {
        console.error("Error fetching banners:", error);
      }
    };
    const fetchCities = async () => {
      try {
        const response = await fetch("https://zhamagat.kz/api/cities/");
        const data = await response.json();
        setCities(data);
      } catch (error) {
        console.error("Error fetching cities:", error);
      }
    };
    if (typeof window !== 'undefined') {
      isSelected = localStorage.getItem('gender')
      setGenderModal(!isSelected)
    }
    fetchBanners();
    fetchCities();
  }, []);



  useEffect(() => {
    const fetchMosques = async () => {
      try {
        const response = await fetch(`https://zhamagat.kz/api/mosques/?city=${selectedCity}`);
        const data = await response.json();

        setMosques(data);
        setIsLoading(false)
      } catch (error) {
        console.error("Error fetching mosques:", error);
      }
    };
    fetchMosques();
  }, [selectedCity]);
  return (
      <Layout className="container">
        <Header style={{backgroundColor:"white",height: 50, alignItems:"center",padding:20}}>
          <div className="search">
            <Input
                bordered={search}
                   onPressEnter={onSearch}
                   onFocus={()=>setSearch(true)}
                   onBlur={()=>setSearch(false)}
                   className={"search_input"}
                   style={search?{width: '90%' }:{width:'5%'}}
            />
          </div>
          <Select
              ref={selectRef}
              defaultValue={1}
              value={selectedCity}
              style={{ width: "90%",maxWidth:130}}
              dropdownStyle={{top:'50px',width:'250px'}}
              dropdownAlign={{ offset: [-80, 2] }}
              popupMatchSelectWidth={false}
              bordered={false}
              onChange={handleChange}
              options={cities.map(city => ({ label: city.name, value: city.id }))}
          />

            <Image
                onClick={()=>{setFilterShow(true)}}
                src={FilterIcon}
                alt="Filter Icon"
                width={30}
                height={30}
            />
        </Header>
        <Content>
          {isLoadingBanner?(
              <Skeleton.Input size={'large'} block={true} style={{margin:"10px",width:"95%"}} active />
          ):(
              <Carousel autoplay className="carousel">
                {images.map((item,index)=>(
                    <div key={`banner-img-${index}`}>
                      <Image className="bannerImg" src={item.image} alt="" width={500}
                             height={500}/>
                    </div>
                ))}
              </Carousel>
          )}

          {isLoading?(
              <>
                <Skeleton.Input size={'default'} block={true} style={{margin:"10px",width:"90%"}} active />
                <Skeleton.Input size={'default'} block={true} style={{margin:"10px",width:"90%"}} active />
                <Skeleton.Input size={'default'} block={true} style={{margin:"10px",width:"90%"}} active />

              </>
          ):(
              <div>
                {mosques.map((item,index)=>(
                    <Mosque key={`mosque list ${index}`} mosqueInput={item}/>
                ))}
              </div>
          )}
          <Modal
              title=""
              open={genderModal}
              className="gender__modal"
              width={400}
              footer={null}
              closable={false}
          >
            <Gender close={(value: boolean) => setGenderModal(value)} />
          </Modal>
        </Content>

        <Filter isOpen={filterShow}
                setIsOpen={setFilterShow}
                cityGiven={cities}
                city={selectedCity}
                setCity={setSelectedCity}
                mosquesGiven = {mosques}
        />
      </Layout>
  );
};
